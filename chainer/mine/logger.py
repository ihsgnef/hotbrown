import datetime
import sys
import time
from time import gmtime, strftime
import logging

class ProgressBar(object):
    def __init__(self, training_length=None, update_interval=100,
            bar_length=50, out=sys.stdout):
        self._training_length = training_length
        self._update_interval = update_interval
        self._bar_length = bar_length
        self._out = out
        self._recent_timing = []

    def __call__(self, data_iter):
        training_length = self._training_length
        length, unit = training_length
        out = self._out
        iteration = data_iter.iteration

        if iteration % self._update_interval == 0:
            epoch = data_iter.epoch_detail
            recent_timing = self._recent_timing
            now = time.time()

            recent_timing.append((iteration, epoch, now))

            out.write('\033[J')

            if unit == 'iteration':
                rate = iteration / length
            else:
                rate = epoch / length

            bar_length = self._bar_length
            marks = '#' * int(rate * bar_length)
            out.write('         total [{}{}] {:6.2%}\n'.format(
            marks, '.' * (bar_length - len(marks)), rate))

            epoch_rate = epoch - int(epoch)
            marks = '#' * int(epoch_rate * bar_length)
            out.write('    this epoch [{}{}] {:6.2%}\n'.format(
            marks, '.' * (bar_length - len(marks)), epoch_rate))

            out.write("{:10} iter, {} epochs / {} {}\n".format(data_iter.iteration,
                    data_iter.epoch, training_length[0], training_length[1]))

            old_t, old_e, old_sec = recent_timing[0]
            span = now - old_sec
            if span != 0:
                speed_t = (iteration - old_t) / span
                speed_e = (epoch - old_e) / span
            else:
                speed_t = float('inf')
                speed_e = float('inf')

            if unit == 'iteration':
                estimated_time = (length - iteration) / speed_t
            else:
                estimated_time = (length - epoch) / speed_e
            out.write('{:10.5g} iters/sec. Estimated time to finish: {}.\n'.format(
                    speed_t, datetime.timedelta(seconds=estimated_time)))

            # move the cursor to the head of the progress bar
            out.write('\033[4A')
            out.flush()

            if len(recent_timing) > 100:
                del recent_timing[0]

    def finalize(self):
        # delete the progress bar
        out = self._out
        out.write('\033[J')
        out.flush()

class StatsLogger:
    def __init__(self, start_time, header):
        self.start_time = start_time
        self.margin = 15
        self.colwidth = 12
        header_string = " " * self.margin
        for entry in header:
            template = "{:<%ds}  " % self.colwidth
            header_string += template.format(entry)
        sys.stdout.write("\033[J")
        sys.stdout.write(header_string + "\n")

    def log(self, *record):
        templates = []
        for i, entry in enumerate(record):
            templates.append((entry, '{:<%dg}  ' % self.colwidth))
        out_string = ""
        for entry, template in templates:
          out_string += template.format(entry)
        elapsed_time = str(datetime.timedelta(
                seconds=time.time() - self.start_time))
        elapsed_time = elapsed_time + " " * (self.margin - len(elapsed_time))

        sys.stdout.write("\033[J")
        sys.stdout.write(elapsed_time + out_string + "\n")
