import six.moves.cPickle as pickle

from vocab import Vocab
from config import *
from multiprocessing import Pool

config = deen_config()

train_src = open(config.train_src_dir).readlines()
train_trg = open(config.train_trg_dir).readlines()
valid_src = open(config.valid_src_dir).readlines()
valid_trg = open(config.valid_trg_dir).readlines()

vocab_src = Vocab()
vocab_trg = Vocab()
vocab_src.create(train_src, config.vocab_size)
vocab_trg.create(train_trg, config.vocab_size)
vocab_src.save(config.vocab_src_dir)
vocab_trg.save(config.vocab_trg_dir)

print('vocab_src.size', vocab_src.size())
print('vocab_trg.size', vocab_trg.size())

def convert_src(sent):
    return vocab_src.sentence2ids(sent)
    
def convert_trg(sent):
    return vocab_trg.sentence2ids(sent)

pool = Pool(8)
_train_src = pool.map(convert_src, train_src)
_train_trg = pool.map(convert_trg, train_trg)
_valid_src = pool.map(convert_src, valid_src)
_valid_trg = pool.map(convert_trg, valid_trg)

train_src = []
train_trg = []
valid_src = []
valid_trg = []

for src, trg in zip(_train_src, _train_trg):
    if len(src) <= config.max_len and len(trg) <= config.max_len:
        train_src.append(src)
        train_trg.append(trg)

for src, trg in zip(_valid_src, _valid_trg):
    if len(src) <= config.max_len and len(trg) <= config.max_len:
        valid_src.append(src)
        valid_trg.append(trg)

print('train size', len(train_src))
print('valid size', len(valid_src))

with open(config.train_src_ids_dir, 'w') as f:
    for sent in train_src:
        for w in sent:
            print(w, end=' ', file=f)
        print('', file=f)

with open(config.train_trg_ids_dir, 'w') as f:
    for sent in train_trg:
        for w in sent:
            print(w, end=' ', file=f)
        print('', file=f)

with open(config.valid_src_ids_dir, 'w') as f:
    for sent in valid_src:
        for w in sent:
            print(w, end=' ', file=f)
        print('', file=f)

with open(config.valid_trg_ids_dir, 'w') as f:
    for sent in valid_trg:
        for w in sent:
            print(w, end=' ', file=f)
        print('', file=f)
