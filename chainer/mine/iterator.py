import numpy as np
import random
import chainer
from chainer import cuda
from chainer import Variable
from collections import defaultdict

from vocab import BOS_ID, EOS_ID, IGNORE_ID

class Seq2SeqIterator(chainer.dataset.Iterator):
    def __init__(self, data_set, config, shuffle=True):
        self.data_set = data_set
        self.config = config
        self.batch_size = config.batch_size
        self.size = len(data_set)
        assert self.batch_size <= self.size
        self.src_bucket_step = config.src_bucket_step
        self.trg_bucket_step = config.trg_bucket_step
        self.shuffle = shuffle

        self.epoch = 0
        self.is_new_epoch = False
        self.iteration = 0
        self.batch_index = 0

        self.batches = self.create_batches()


    def create_batches(self):
        batches = []
        buckets = defaultdict(list)
        for src_ids, trg_ids in self.data_set:
            while len(src_ids) % self.src_bucket_step > 0:
                src_ids.append(IGNORE_ID)
            # we don't append BOS here because it's never predicted
            trg_ids = trg_ids + [EOS_ID]
            while len(trg_ids) % self.trg_bucket_step > 0:
                trg_ids.append(IGNORE_ID)
            buckets[len(src_ids), len(trg_ids)].append((src_ids, trg_ids))
        for samples in buckets.values():
            for i in range(0, len(samples), self.batch_size):
                src_ids_lst, trg_ids_lst = zip(*samples[i:i+self.batch_size])
                src_ids_arr = np.asarray(src_ids_lst, dtype=np.int32)
                trg_ids_arr = np.asarray(trg_ids_lst, dtype=np.int32)
                batch = src_ids_arr, trg_ids_arr
                batches.append(batch)
        return batches


    def next_batch(self):
        self.iteration += 1
        self.is_new_epoch = (self.batch_index == 0)
        if self.is_new_epoch:
            self.epoch += 1
            if self.shuffle:
                random.shuffle(self.batches)

        # [(batch_size, dim)]
        xs_data, ts_data = self.batches[self.batch_index]
        self.batch_index = (self.batch_index + 1) % len(self.batches)

        if self.config.gpu is not None:
                xs_data = cuda.to_gpu(xs_data)
                ts_data = cuda.to_gpu(ts_data)

        x_batch = Variable(xs_data, volatile='off')
        y_batch = Variable(ts_data, volatile='off')
        return x_batch, y_batch

    @property
    def epoch_detail(self):
        return self.epoch + (self.batch_index * 1.0 / len(self.batches))

    def serialize(self, serializer):
        self.iteration = serializer("iteration", self.iteration)
        self.epoch = serializer("self.epoch", self.epoch)
        self.batch_index = serializer("self.offset", self.batch_index)
