import six.moves.cPickle as pickle

IGNORE_ID = -1
BOS_ID = 0
EOS_ID = 1
UNK_ID = 2
BOS = "<BOS>"
EOS = "<EOS>"
UNK = "<UNK>"

class Vocab(object):

    def create(self, data, vocab_size):
        '''
        build a  vocabulary of size vocab_size on data
        Args:
            data
                a list of sentences where each setence is a list of words
            vocab_size
                an integer
        '''
        word_count = {}
        for sent in data:
            sent = sent.strip().lower().split()
            for word in sent:
                if word not in word_count:
                    word_count[word] = 0
                word_count[word] += 1
        words = sorted(word_count.items(), key=lambda x: x[1])
        self.i2w = [BOS, EOS, UNK]
        self.i2w += [x[0] for x in words[::-1][:vocab_size - 3]]
        self.w2i = {w:i for i, w in enumerate(self.i2w)}

    def size(self):
        return len(self.i2w)

    def word2id(self, word):
        return self.w2i.get(word, UNK_ID)
    
    def id2word(self, i):
        return self.i2w[i]

    def sentence2ids(self, sentence):
        sentence = sentence.lower().split()
        sentence = sentence
        return [self.word2id(w) for w in sentence] 

    def ids2sentence(self, ids):
        return ' '.join([self.id2word(i) for i in ids])

    def save(self, path):
        pickle.dump((self.i2w, self.w2i), open(path, 'wb'))

    def load(self, path):
        self.i2w, self.w2i = pickle.load(open(path, 'rb'))

