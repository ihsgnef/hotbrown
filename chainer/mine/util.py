import os
import random
import sys
import signal
import copy
import six.moves.cPickle as pickle
import numpy as np
from collections import defaultdict

from chainer.serializers import save_hdf5, load_hdf5
from chainer.serializers import save_npz, load_npz
from chainer import Variable

from vocab import Vocab
from vocab import BOS_ID, EOS_ID, IGNORE_ID


def load_ids_file(file_path):
    data_set = []
    for line in open(file_path, 'r').readlines():
        sent = [int(x) for x in line.strip().split(' ')]
        data_set.append(sent)
    return data_set
        

def load_data(config):
    vocab_src = Vocab()
    vocab_src.load(config.vocab_src_dir)
    vocab_trg = Vocab()
    vocab_trg.load(config.vocab_trg_dir)
    sys.stdout.flush()

    train_src = load_ids_file(config.train_src_ids_dir)
    train_trg = load_ids_file(config.train_trg_ids_dir)
    valid_src = load_ids_file(config.valid_src_ids_dir)
    valid_trg = load_ids_file(config.valid_trg_ids_dir)

    train_set = list(zip(train_src, train_trg))
    valid_set = list(zip(valid_src, valid_trg))
    return train_set, valid_set, vocab_src, vocab_trg


def flipCoin(p):
    r = random.random()
    return r > p


def save_model_def(model, config):
    ''' save the model without actual data '''
    obj = copy.deepcopy(model)
    for p in obj.params():
        p.data = None
    with open(config.model_def_path, 'wb') as f:
        pickle.dump(model, f)


def save_model(model, config):
    assert os.path.exists(config.model_def_path), 'Must call save_model_def() first'
    path = config.model_path#  + '.{}'.format(epoch)
    save_npz(path, model)


def load_model(config):
    with open(config.model_def_path, 'rb') as f:
        model = pickle.load(f)
        load_npz(config.model_path, model)
    return model


def gen_toy_data(train_size, valid_size, max_length):
  word_id_map = {"<PAD>" : 0, "<EOS>": 1, "a" : 2, "b" : 3, "c" : 4, "d" : 5}
  vocab_size = len(word_id_map)
  np.random.seed(234)
  random.seed(543)
  def sample_data(min_length, max_length):
    x = np.random.randint(2, vocab_size, size=(random.randint(min_length,
        max_length),)).tolist()
    y = x[::-1]
    return x, y
  train_set = [sample_data(1, max_length) for _ in range(train_size)]
  valid_set = [sample_data(1, max_length) for _ in range(valid_size)]
  return train_set, valid_set, vocab_size, vocab_size
