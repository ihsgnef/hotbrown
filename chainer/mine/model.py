import numpy as np
import chainer
from chainer import Variable, Chain, ChainList, cuda
import chainer.functions as F
import chainer.links as L

from vocab import BOS_ID, EOS_ID, IGNORE_ID
from util import *


class Seq2SeqModel(chainer.Chain):

    def __init__(self, model):
        super(Seq2SeqModel, self).__init__(model=model)

    def __call__(self, source, targets, loss_func, metric_func, **kwargs):
        self.model.reset_state()
        enc_states = self.model.encode(source)
        self.model.attention.precompute(enc_states)
        preds, probs, states = self.model.decode(enc_states[-1], targets, **kwargs)

        batch_size, input_length, _ = states.shape
        xp = cuda.get_array_module(states.data)

        loss = loss_func(targets, preds, probs, states)
        reward = metric_func(targets.data.tolist(), preds.data.tolist())
        metric = sum(reward) / batch_size
        
        loss = F.sum(loss) / (batch_size * input_length)
        return loss, metric

    def generate(self, source, targets, max_len, loss_func, metric_func, **kwargs):
        self.model.reset_state()
        enc_states = self.model.encode(source)
        self.model.attention.precompute(enc_states)
        preds, probs, states = self.model.generate(enc_states[-1], max_len, **kwargs)

        batch_size, input_length, _ = states.shape
        xp = cuda.get_array_module(states.data)

        # when generation, use predicted labels to calculate loss
        loss = loss_func(preds, preds, probs, states)
        reward = metric_func(targets.data.tolist(), preds.data.tolist())
        metric = sum(reward) / batch_size

        loss = F.sum(loss) / (batch_size * input_length)
        return loss, metric
