import numpy as np
import chainer
from chainer import Variable, Chain, ChainList, cuda
import chainer.functions as F
import chainer.links as L

from vocab import BOS_ID, EOS_ID, IGNORE_ID
from util import *


class StackRNN(ChainList):

    def __init__(self, 
                 num_layers, 
                 input_dim, 
                 hidden_dim, 
                 dropout_ratio=1,
                 use_gru=False):

        """
        args:
            num_layers
                number of stacked LSTM layers
            input_dim
                dimension of input vectors, normally word embeddings
            hidden_dim
                size of the hidden state vector of each LSTM layer
            dropout_ratio
                dropout between each layer
            use_gru
                use gru instead of lstm
        """
        super(StackRNN, self).__init__()

        self.num_layers    = num_layers
        self.input_dim     = input_dim
        self.hidden_dim    = hidden_dim
        self.dropout_ratio = dropout_ratio

        if use_gru:
            self.add_link(L.StatefulGRU(input_dim, hidden_dim))
            for i in range(1, num_layers):
                self.add_link(L.StatefulGRU(hidden_dim, hidden_dim))
        else:
            self.add_link(L.LSTM(input_dim, hidden_dim))
            for i in range(1, num_layers):
                self.add_link(L.LSTM(hidden_dim, hidden_dim))

    def reset_state(self):
        """
        reset the states of each LSTM layer
        """
        for i in range(self.num_layers):
            self[i].reset_state()

    def step(self, x, train=True):
        """
        run one step
        """
        # FIXME how to dropout properly?
        for i in range(self.num_layers):
            if self.dropout_ratio < 1:
                x = F.dropout(x, ratio=self.dropout_ratio, train=train)
            x = self[i](x)
        return x

    def __call__(self, xs, train=True):
        """
        given a list of input vectors, return a list of hidden states of the top
        LSTM layer.
        """
        hs = []
        for x in xs:
            hs.append(self.step(x, train))
        return hs 


class BiRNN(Chain):
    """
    Bi-directional RNN
    the output is the concatenation of forward and backward RNN hidden states
    """

    def __init__(self, fwd_rnn, bwd_rnn):
        super(BiRNN, self).__init__(
            fwd_rnn=fwd_rnn,
            bwd_rnn=bwd_rnn
        )
        self.hidden_dim = fwd_rnn.hidden_dim + bwd_rnn.hidden_dim

    def reset_state(self):
        self.fwd_rnn.reset_state()
        self.bwd_rnn.reset_state()

    def __call__(self, xs, train=True):
        # [(batch, dim)]
        hs_fwd = self.fwd_rnn(xs, train)
        hs_bwd = reversed(self.bwd_rnn(xs[::-1], train))
        hs = [F.concat([fh, bh], axis=1) for fh, bh in zip(hs_fwd, hs_bwd)]
        return hs


class Encoder(Chain):

    def __init__(self, 
                 word_embedding,
                 embedding_dim,
                 enc_num_layers, 
                 enc_hidden_dim,
                 dropout_ratio=1,
                 use_gru=False,
                 use_enc_birnn=True):
        '''
        args:
            word_embedding
                source word embeddings
            embedding_dim
                dimension of word embedding
            enc_num_layers 
                number of layers of encoder rnn
            enc_hidden_dim
                dimension of encoder hidden states
            dropout_ratio
                dropout ratio of encoder rnn
            use_gru
                use gru instead of lstm
            use_enc_birnn
                use bidirectional rnn or not
        '''
        if use_enc_birnn:
            encoder_fwd = StackRNN(enc_num_layers,
                                   embedding_dim,
                                   enc_hidden_dim,
                                   dropout_ratio,
                                   use_gru)
            encoder_bwd = StackRNN(enc_num_layers,
                                   embedding_dim,
                                   enc_hidden_dim,
                                   dropout_ratio,
                                   use_gru)
            rnn = BiRNN(encoder_fwd, encoder_bwd)
        else:
            rnn = StackRNN(enc_num_layers,
                           embedding_dim,
                           enc_hidden_dim,
                           dropout_ratio,
                           use_gru)
        self.word_embedding = word_embedding
        super(Encoder, self).__init__(rnn=rnn)

    def reset_state(self):
        self.rnn.reset_state()

    def __call__(self, xs, train=True):
        hs = F.swapaxes(self.word_embedding(xs), 0, 1)
        hs = self.rnn(hs, train)
        return hs


class Attention(Chain):

    def __init__(self,
                 enc_hidden_dim,
                 dec_hidden_dim,
                 attention_dim):

        super(Attention, self).__init__(
            # transform encoder hidden states
            linear_enc=L.Linear(enc_hidden_dim, attention_dim),
            # transform decoder hidden states
            linear_dec=L.Linear(dec_hidden_dim, attention_dim),
            # transform the matching matrix to unnormalized weights
            linear_att=L.Linear(attention_dim, 1)
        )
        self.enc_hidden_dim = enc_hidden_dim
        self.dec_hidden_dim = dec_hidden_dim
        self.attention_dim = attention_dim

    def precompute(self, enc_states):
        '''
        pre-computation
        convert the list of encoder states to attended state matrix
        from enc_hidden_dim to attention_dim
        '''
        batch_size, dim = enc_states[0].shape
        # (batch_size, length, dim)
        self.enc_states = F.swapaxes(F.stack(enc_states), 0, 1)
        # (batch_size * input_length, dim)
        attended = self.linear_enc(F.reshape(self.enc_states, (-1, dim)))
        # (batch_size, input_length, dim)
        self.attended = F.tanh(F.reshape(attended, 
                                        (batch_size, -1, self.attention_dim)))

    def __call__(self, dec_state):
        '''
        args:
            dec_state
        output:
            context vector and weights
        process:
            use the encoder state matrix and the previous decoder state to
            calculate the attention weights on the encoder states, use the
            weights to get the context vector, feed the concatenation of the
            context vector and the previous word to the decoder rnn, use the new
            decoder state for prediction
        '''
        batch_size, input_length, attention_dim = self.attended.shape
        # (batch_size, attention_dim)
        dec_state = self.linear_dec(dec_state)
        # (batch_size, 1, attention_dim)
        dec_state = F.expand_dims(dec_state, axis=1)
        # (batch_size, input_length, attention_dim)
        dec_state = F.broadcast_to(dec_state, (batch_size, input_length, attention_dim))

        # (batch_size, input_length, attention_dim)
        match = F.tanh(dec_state + self.attended)
        match = F.reshape(match, (batch_size * input_length, attention_dim))
        # (batch_size, input_length, 1)
        w = self.linear_att(match)
        w = F.reshape(w, (batch_size, input_length))
        w = F.softmax(w)

        # (batch_size, attention_dim)
        context = F.reshape(F.batch_matmul(self.enc_states, w, transa=True), 
                            (batch_size, self.enc_hidden_dim))  
        return context, w


class Decoder(Chain):

    def __init__(self, 
                 attention,
                 word_embedding, 
                 embedding_dim,
                 dec_num_layers,
                 enc_hidden_dim, 
                 dec_hidden_dim, 
                 output_dim,
                 dropout_ratio=1,
                 use_gru=False,
                 use_zero_init=True,
                 use_dagger=False):
        '''
        args:
            attention
                the attention unit with two functions, precompute and __call__
            word_embedding
                target word embeddings
            embedding_dim
                dimension of word embedding
            dec_num_layers
                number of layers in decoder rnn
            enc_hidden_dim
                dimension of encoder hidden states, if encoder is bidirectional,
                this is already doubled
            dec_hidden_dim
                dimension of decoder hidden ates
            output_dim
                output vocabulary size
            dropout_ratio
                dropout ratio of decoder rnn
            use_gru
                use gru instead of lstm
            use_zero_init
                if true use zero initial state, otherwise initialize with
                enc_state after linear transformation
            use_dagger
                to use dagger or scheduled sampling
        '''
        self.attention      = attention
        self.word_embedding = word_embedding
        self.embedding_dim  = embedding_dim
        self.dec_num_layers = dec_num_layers
        self.enc_hidden_dim = enc_hidden_dim
        self.dec_hidden_dim = dec_hidden_dim
        self.output_dim     = output_dim
        self.dropout_ratio  = dropout_ratio
        self.use_zero_init  = use_zero_init
        self.use_dagger     = use_dagger

        rnn = StackRNN(dec_num_layers,
                       # feed the concat of previous word and context
                       embedding_dim + enc_hidden_dim,
                       dec_hidden_dim,
                       dropout_ratio,
                       use_gru)

        super(Decoder, self).__init__(
            rnn=rnn,
            softmax_linear=L.Linear(rnn.hidden_dim, output_dim)
        )
        if use_zero_init is False:
            # use transformed encoder state to initialize decoder state
            self.add_link('init_state_linear',
                    L.Linear(enc_hidden_dim, dec_hidden_dim))

    def reset_state(self):
        self.rnn.reset_state()

    def __call__(self, ts, dec_init, dagger_epsilon=1):
        '''
        args:
            ts
                the target sequence, no bos appended, a list of variables
            dec_init 
                used for initializing decoder hidden state
            dagger_epsilon
                hyperparameter of dagger
        outputs:
            ys
                list of weight outputs, [(length, vocab_size)] * batch_size
            ws
                list of attention weights, [(length, src length)] * batch_size
            hs
                list of decoder states, [(length, dec_hidden_dim)] * batch_size
        '''
        ys = []
        ws = []
        hs = []

        xp = cuda.get_array_module(dec_init.data)
        batch_size = dec_init.shape[0]

        if self.use_zero_init:
            h = self.zero_init_states(xp, batch_size)
        else:
            h = self.init_state_linear(dec_init)
        y = None
        bos = F.expand_dims(self.bos(xp, batch_size), 0)
        for i, x in enumerate(F.vstack([bos, ts])):
            if i > 0 and self.use_dagger and dagger_epsilon < 1:
                if flipCoin(dagger_epsilon):
                    x = y.data.argmax(axis=1).astype(xp.int32)

            # predictions, attention weights, new state
            y, w, h = self.step(x, h, train=True)
            ys.append(y)
            ws.append(w)
            hs.append(h)

        ys = F.stack(F.transpose_sequence(ys))
        ws = F.stack(F.transpose_sequence(ws))
        hs = F.stack(F.transpose_sequence(hs))
        return ys, ws, hs 

    def generate(self, dec_init, max_len, temp=1.):
        '''
        args:
            dec_init 
                the last encoder state
            max_len
                maximum length of generated sentence
            temp
                temperature of softmax
        outputs:
            ys
                list of weight outputs, [(length, vocab_size)] * batch_size
            ws
                list of attention weights, [(length, src length)] * batch_size
            hs
                list of decoder states, [(length, dec_hidden_dim)] * batch_size
        '''
        ys = []
        ws = []
        hs = []
        xp = cuda.get_array_module(dec_init)
        batch_size = dec_init.shape[0]
        if self.use_zero_init:
            h = self.zero_init_states(xp, batch_size)
        else:
            h = self.init_state_linear(dec_init)
        done = xp.asarray([False] * batch_size)
        x = self.bos(xp, batch_size)
        for i in range(max_len):
            y, w, h = self.step(x, h, train=False)
            ys.append(y)
            ws.append(w)
            hs.append(h)

            x = F.argmax(y, axis=1)
            done |= x.data == EOS_ID
            if all(done):
                break

        ys = F.stack(F.transpose_sequence(ys))
        ws = F.stack(F.transpose_sequence(ws))
        hs = F.stack(F.transpose_sequence(hs))
        return ys, ws, hs 

    def zero_init_states(self, xp, batch_size):
        d = xp.zeros((batch_size, self.dec_hidden_dim), dtype=xp.float32)
        v = Variable(d, volatile='auto')
        return d
    
    def bos(self, xp, batch_size):
        return Variable(xp.full((batch_size,), BOS_ID, dtype=xp.int32), 
                        volatile='auto')
        

    def step(self, x, h, train=True):
        '''
        args:
            x
                previous target word (predicted or ground truth)
            h
                the previous decoder state
        output:
            y
                predicted weights on the next target word
            w
                attention weights on the encoder states
            h 
                the new decoder state
        process:
            use the encoder state matrix and the previous decoder state to
            calculate the attention weights on the encoder states, use the
            weights to get the context vector, feed the concatenation of the
            context vector and the previous word to the decoder rnn, use the new
            decoder state for prediction
        '''
        x = self.word_embedding(x) 
        context, w = self.attention(h)
        # (batch, enc_hidden_dim + embedding_dim)
        dec_input = F.concat([context, x], axis=1)
        h = self.rnn.step(dec_input, train)
        y = self.softmax_linear(h)
        return y, w, h 


class EncoderDecoder(Chain):

    def __init__(self, 
                 src_vocab_size,
                 trg_vocab_size,
                 embedding_dim,
                 enc_num_layers, 
                 dec_num_layers,
                 enc_hidden_dim,
                 dec_hidden_dim,
                 attention_dim,
                 dropout_ratio=1,
                 use_gru=False,
                 use_enc_birnn=True,
                 use_zero_init=True,
                 use_dagger=False):

        self.hidden_dim = dec_hidden_dim

        src_embedding=L.EmbedID(src_vocab_size, embedding_dim,
                                ignore_label=IGNORE_ID)
        trg_embedding=L.EmbedID(trg_vocab_size, embedding_dim,
                                ignore_label=IGNORE_ID)

        encoder = Encoder(src_embedding,
                          embedding_dim,
                          enc_num_layers,
                          enc_hidden_dim,
                          dropout_ratio,
                          use_gru,
                          use_enc_birnn)
        if use_enc_birnn:
            enc_hidden_dim *= 2

        attention = Attention(enc_hidden_dim,
                              dec_hidden_dim,
                              attention_dim)

        decoder = Decoder(attention,
                          trg_embedding,
                          embedding_dim,
                          dec_num_layers,
                          enc_hidden_dim,
                          dec_hidden_dim,
                          trg_vocab_size,
                          dropout_ratio,
                          use_gru,
                          use_zero_init,
                          use_dagger)
        
        super(EncoderDecoder, self).__init__(
            src_embedding=src_embedding,
            trg_embedding=trg_embedding,
            encoder=encoder,
            decoder=decoder,
            attention=attention
        )

    def reset_state(self):
        self.encoder.reset_state()
        self.decoder.reset_state()
        
    def encode(self, xs):
        enc_states = self.encoder(xs, train=True)
        return enc_states

    def decode(self, dec_init, ts, dagger_epsilon=1):
        ts = F.swapaxes(ts, 0, 1)
        ys, ws, hs = self.decoder(ts[:-1], dec_init, dagger_epsilon)
        predictions = F.argmax(ys, axis=2)
        return predictions, ys, hs

    def generate(self, dec_init, max_len, temp=1.):
        ys, ws, hs = self.decoder.generate(dec_init, max_len, temp=temp)
        predictions = F.argmax(ys, axis=2)
        return predictions, ys, hs
