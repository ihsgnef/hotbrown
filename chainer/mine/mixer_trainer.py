import sys
import time
from logger import *
from vocab import BOS_ID, EOS_ID, IGNORE_ID
from util import save_model
import chainer.functions as F


class MixerTrainer(object):
    def __init__(self, config, optimizer, mixer, metric, evaluator):
        self.config = config
        self.optimizer = optimizer
        self.model = optimizer.target
        self.mixer = mixer
        self.metric = metric
        self.evaluator = evaluator
        self.dagger_epsilon = 1

    def _reset(self):
        self.epoch_loss = 0.
        self.epoch_metric = 0.
        self.num_batches = 0.

    def epoch_end(self, logger, train_iter, valid_iter):
        train_epoch_loss = self.epoch_loss / self.num_batches
        train_epoch_metric = self.epoch_metric / self.num_batches
        # logging
        if self.config.evaluation is True:
            valid_loss, valid_metric = self.evaluator.run(valid_iter)
            logger.log(train_iter.epoch, 
                       train_epoch_loss, train_epoch_metric, 
                       valid_loss, valid_metric)
        else:
            logger.log(train_iter.epoch, train_epoch_loss, train_epoch_metric)
        # save model
        save_model(self.model, self.config)

        # update dagger epsilon
        if self.config.use_dagger is True:
            if train_iter.epoch >= self.config.dagger_start:
                # linear decay
                self.dagger_epsilon -= 0.01


    def run(self, train_iter, valid_iter, num_epochs):
        self._reset()
        self.loss_func = self.mixer.xent_loss
        logger = StatsLogger(time.time(), 
                ["Epoch", "Train loss", "Train metric", 
                 "Valid loss",  "Valid metric"])
        progress_bar = ProgressBar((num_epochs, "epochs"), update_interval=1)
        while True:
            xs, ts = train_iter.next_batch()
            t_len = len(ts)
            batch_size = xs[0].shape[0]
            # if not use_dagger, dagger_epsilon will always be 1 and we won't
            # flip the coin in the model
            loss, metric = self.model(xs, ts, self.loss_func, self.metric,
                    dagger_epsilon=self.dagger_epsilon)

            self.epoch_loss += loss.data
            self.epoch_metric += metric
            self.num_batches += 1

            self.model.cleargrads()
            loss.backward()
            self.optimizer.update()

            progress_bar(train_iter)

            if train_iter.is_new_epoch:
                self.epoch_end(logger, train_iter, valid_iter)
                if train_iter.epoch >= num_epochs:
                    progress_bar.finalize()
                    break
                self._reset()
