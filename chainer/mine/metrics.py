import numpy as np
import chainer
import chainer.functions as F
import chainer.links as L
from chainer import Variable
import nltk.translate.bleu_score as nltk_bleu
from vocab import BOS_ID, EOS_ID, IGNORE_ID

'''
reward functions
args:
    targets, preds
'''

def sentence_bleu(target, pred):
    target = clean_up_sentence(target)
    pred = clean_up_sentence(pred)
    scores = nltk_bleu.sentence_bleu([target], pred)
    return scores

def clean_up_sentence(sent):
    try:
        sent = sent[:(sent.index(EOS_ID) + 1)]
    except ValueError:
        pass
    if sent[-1] != EOS_ID:
        sent.append(EOS_ID)
    return sent

def multiple_sentences_bleu(targets, preds):
    scores = []
    for target, pred in zip(targets, preds):
        scores.append(sentence_bleu(target, pred))
    return scores

def accuracy(targets, preds):
    accuracies = []
    for target, pred in zip(targets, preds):
        target = clean_up_sentence(target)
        pred = clean_up_sentence(pred)
        total = 0
        correct = 0.0
        for i in range(len(target)):
            if target[i] is IGNORE_ID:
                continue
            total += 1
            if i < len(pred):
                correct += target[i] == pred[i]
        if total is not 0:
            accuracy = correct / total
        else:
            accuracy = 0
        accuracies.append(accuracy)
    return accuracies


'''
loss functions
args:
    targets, preds, probs, states
'''

def softmax_cross_entropy(targets, preds, probs, states):
    batch_size, input_length, _ = probs.shape
    probs = F.reshape(probs, (batch_size * input_length, -1))
    targets = F.reshape(targets, (batch_size * input_length, ))
    loss = -F.select_item(F.log_softmax(probs), targets)
    loss = F.reshape(loss, (batch_size, input_length))
    return loss


class MixerLoss(chainer.Chain):

    def __init__(self, hidden_dim):
        self.regressor = L.Linear(hidden_dim, 1)
        self.optimizer = chainer.optimizers.Adam()
        self.optimizer.setup(self.regressor)

    def train_regressor(self, hs_data, reward):
        hs = Variable(hs_data)
        ts = Variable(reward)
        output = F.flatten(self.regressor(hs))
        loss = F.mean_squared_error(ts, output)
        diff = (ts - output).data
        self.regressor.cleargrads()
        loss.backward()
        # print(self.regressor.W.grad)
        self.optimizer.update()
        return diff

    def xent_loss(self, targets, preds, probs, states):
        batch_size, input_length, _ = states.shape
        reward = multiple_sentences_bleu(preds.data.tolist(), targets.data.tolist())
        reward = np.asarray(reward, dtype=np.float32)
        reward = np.broadcast_to(reward, (input_length, batch_size))
        reward = reward.transpose().ravel()
        states = np.reshape(states.data, (batch_size * input_length, -1))
        assert states.shape[:1] == reward.shape
        diff = self.train_regressor(states, reward)
        return softmax_cross_entropy(targets, preds, probs, states)
    
    def reinforce_loss(self, targets, preds, probs, states):
        batch_size, input_length, _ = states.shape
        loss = softmax_cross_entropy(targets, preds, probs, states)
        reward = multiple_sentences_bleu(preds.data.tolist(), targets.data.tolist())
        reward = np.asarray(reward, dtype=np.float32)
        reward = np.broadcast_to(reward, (input_length, batch_size))
        reward = reward.transpose().ravel()
        states = np.reshape(states.data, (batch_size * input_length, -1))
        assert states.shape[:1] == reward.shape
        diff = self.train_regressor(states, reward)
        diff = np.reshape(diff, (batch_size, input_length))
        assert diff.shape == loss.shape
        loss *= diff
        return loss
