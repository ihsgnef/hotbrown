import os
import logging
from config import *

import chainer
import chainer.functions as F

import metrics
from vocab import Vocab
from util import *
from encdec import *
from model import *
from iterator import *
from trainer import *
from mixer_trainer import *

def main():
    config = toy_config()

    if not os.path.isdir(config.model_dir):
        os.makedirs(config.model_dir)

    if not os.path.isdir(config.log_dir):
        os.makedirs(config.log_dir)

    logger = logging.getLogger()
    logging.basicConfig(level=logging.INFO)
    file_handler = logging.FileHandler(config.log_path)
    fmt = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    file_handler.setFormatter(fmt)
    logger.addHandler(file_handler)

    ''' data '''
    # train_set, valid_set, vocab_src, vocab_trg = load_data(config)
    train_set, valid_set, vocab_src, vocab_trg = gen_toy_data(3000, 200,
            config.max_len)
    train_iter = Seq2SeqIterator(train_set, config, shuffle=True)
    valid_iter = Seq2SeqIterator(valid_set, config, shuffle=False)

    ''' model '''
    model = EncoderDecoder(config.vocab_size,
                           config.vocab_size,
                           config.embedding_dim,
                           config.enc_num_layers,
                           config.dec_num_layers,
                           config.enc_hidden_dim,
                           config.dec_hidden_dim,
                           config.attention_dim,
                           config.dropout_ratio,
                           config.use_gru,
                           config.use_enc_birnn,
                           config.use_zero_init,
                           config.use_dagger)
    model = Seq2SeqModel(model)

    ''' load model or save model definition '''
    if config.resume_training:
        model = load_model(config)
    else:
        save_model_def(model, config)

    ''' move model to gpu '''
    if config.gpu is not None:
        model.to_gpu()

    ''' optimizer '''
    optimizer = getattr(chainer.optimizers, config.optimizer)()
    optimizer.use_cleargrads()
    optimizer.setup(model)
    optimizer.add_hook(chainer.optimizer.GradientClipping(config.gradient_clip))

    ''' trainer '''
    evaluator = Evaluator(config, model, 
                          metrics.softmax_cross_entropy, 
                          metrics.multiple_sentences_bleu)
    trainer = Trainer(config,
                      optimizer, 
                      metrics.softmax_cross_entropy,
                      metrics.multiple_sentences_bleu,
                      evaluator=evaluator)
    trainer.run(train_iter, valid_iter, config.num_epochs)


if __name__ == '__main__':
    main()
