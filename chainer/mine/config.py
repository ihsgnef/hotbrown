class giga_fren_config:

    def __init__(self):

        self.gpu             = None
        self.test            = False
        self.resume_training = False
        self.save_model      = True
        self.evaluation      = True

        ''' training settings '''
        self.num_epochs      = 80
        self.batch_size      = 10
        self.save_every      = 1
        self.src_bucket_step = 4
        self.trg_bucket_step = 4
        self.optimizer       = 'AdaDelta'
        self.gradient_clip   = 5
        self.learning_rate   = 0.1
        self.use_dagger      = False
        self.dagger_start    = 20
        self.use_mixer       = False
        self.mixer_start     = 20
    
        ''' model specs '''
        self.use_gru        = False
        self.use_enc_birnn  = True
        self.use_zero_init  = False
        self.enc_num_layers = 1
        self.dec_num_layers = 1
        self.enc_hidden_dim = 64
        self.dec_hidden_dim = 64
        self.attention_dim  = 64
        self.embedding_dim  = 300
        self.dropout_ratio  = 0.2
    
        ''' data size '''
        self.vocab_size       = 20000
        self.max_len = 80
    
        ''' paths '''
        self.model_dir          = "./models/"
        self.model_path         = self.model_dir + "giga_fren.npz"
        self.model_def_path     = self.model_dir + 'giga_fren.def'
        self.log_dir            = './log/'
        self.log_path           = self.log_dir + 'giga_fren.log'

        self.data_dir           = "data/giga-fren/"
        self.train_src_dir      = self.data_dir + "train.fr"
        self.train_trg_dir      = self.data_dir + "train.en"
        self.valid_src_dir      = self.data_dir + "valid.fr"
        self.valid_trg_dir      = self.data_dir + "valid.en"
        self.train_src_ids_dir  = self.data_dir + "train_fr.ids"
        self.train_trg_ids_dir  = self.data_dir + "train_en.ids"
        self.valid_src_ids_dir  = self.data_dir + "valid_fr.ids"
        self.valid_trg_ids_dir  = self.data_dir + "valid_en.ids"
        self.vocab_src_dir      = self.data_dir + "en.vocab"
        self.vocab_trg_dir      = self.data_dir + "fr.vocab"


class deen_config:

    def __init__(self):

        self.gpu             = None
        self.resume_training = False
        self.save_model      = True
        self.evaluation      = True

        ''' training settings '''
        self.num_epochs      = 80
        self.batch_size      = 10
        self.save_every      = 1
        self.src_bucket_step = 4
        self.trg_bucket_step = 4
        self.optimizer       = 'AdaDelta'
        self.gradient_clip   = 5
        self.learning_rate   = 0.1
        self.use_dagger      = False
        self.dagger_start    = 20
        self.use_mixer       = False
        self.mixer_start     = 20
    
        ''' model specs '''
        self.use_gru        = False
        self.use_enc_birnn  = True
        self.use_zero_init  = False
        self.enc_num_layers = 1
        self.dec_num_layers = 1
        self.hidden_dim     = 64
        self.enc_hidden_dim = 64
        self.dec_hidden_dim = 64
        self.attention_dim  = 64
        self.embedding_dim  = 300
        self.dropout_ratio  = 0.2
    
        ''' data size '''
        self.vocab_size       = 40000
        self.max_len = 80
    
        ''' paths '''
        self.model_dir          = "./models/"
        self.model_path         = self.model_dir + "deen.npz"
        self.model_def_path     = self.model_dir + 'deen.def'
        self.log_dir            = './log/'
        self.log_path           = self.log_dir + 'deen.log'

        self.data_dir           = "data/deen/"
        self.train_src_dir      = self.data_dir + "train.de-en.de"
        self.train_trg_dir      = self.data_dir + "train.de-en.en"
        self.valid_src_dir      = self.data_dir + "valid.de-en.de"
        self.valid_trg_dir      = self.data_dir + "valid.de-en.en"
        self.train_src_ids_dir  = self.data_dir + "train.de-en.de.ids"
        self.train_trg_ids_dir  = self.data_dir + "train.de-en.en.ids"
        self.valid_src_ids_dir  = self.data_dir + "valid.de-en.de.ids"
        self.valid_trg_ids_dir  = self.data_dir + "valid.de-en.en.ids"
        self.vocab_src_dir      = self.data_dir + "vocab.de-en.de"
        self.vocab_trg_dir      = self.data_dir + "vocab.de-en.en"
        

class toy_config:

    def __init__(self):

        self.gpu             = None
        self.test            = False
        self.resume_training = False
        self.save_model      = True
        self.evaluation      = True

        ''' training settings '''
        self.num_epochs      = 100
        self.batch_size      = 32
        self.save_every      = 1
        self.src_bucket_step = 4
        self.trg_bucket_step = 4
        self.optimizer       = 'Adam'
        self.gradient_clip   = 5
        self.learning_rate   = 1.0
        self.use_dagger      = False
        self.dagger_start    = 20
        self.use_mixer       = False
        self.mixer_start     = 20
    
        ''' model specs '''
        self.use_gru        = False
        self.use_enc_birnn  = False
        self.use_zero_init  = False
        self.enc_num_layers = 1
        self.dec_num_layers = 1
        self.enc_hidden_dim = 64
        self.dec_hidden_dim = 64
        self.attention_dim  = 64
        self.embedding_dim  = 64
        self.dropout_ratio  = 0.5
    
        ''' data size '''
        self.vocab_size       = 10
        self.max_len = 15
    
        ''' paths '''
        self.model_dir          = "./models/"
        self.model_path         = self.model_dir + "toy.npz"
        self.model_def_path     = self.model_dir + 'toy.def'
        self.log_dir            = './log/'
        self.log_path           = self.log_dir + 'toy.log'

        self.data_dir           = "data/chainer_char/"
        self.train_src_dir      = self.data_dir + "train.fr"
        self.train_trg_dir      = self.data_dir + "train.en"
        self.valid_src_dir      = self.data_dir + "valid.fr"
        self.valid_trg_dir      = self.data_dir + "valid.en"
        self.train_src_ids_dir  = self.data_dir + "train_fr.ids"
        self.train_trg_ids_dir  = self.data_dir + "train_en.ids"
        self.valid_src_ids_dir  = self.data_dir + "valid_fr.ids"
        self.valid_trg_ids_dir  = self.data_dir + "valid_en.ids"
        self.vocab_src_dir      = self.data_dir + "en.vocab"
        self.vocab_trg_dir      = self.data_dir + "fr.vocab"
