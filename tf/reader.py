import os
import collections
import itertools

import numpy as np
import tensorflow as tf

def _read_words(filename):
    with tf.gfile.GFile(filename, 'r') as f:
        return f.read().decode('utf-8').replace("\n", "<eos>").split()

def _build_vocab(filename):
    data = _read_words(filename)
    counter = collections.Counter(data)
    word_freq_pairs = sorted(counter.items(), key=lambda x: (-x[1], x[0]))
    words = [x for x, y in word_freq_pairs]
    word_to_id = dict(zip(words, range(len(words))))
    return word_to_id

def _file_to_word_ids(filename, word_to_id):
    data = _read_words(filename)
    return [word_to_id[word] for word in data if word in word_to_id]

def ptb_raw_data(data_path=None):
    train_path = os.path.join(data_path, "ptb.train.txt")
    test_path = os.path.join(data_path, "ptb.test.txt")
    valid_path = os.path.join(data_path, "ptb.valid.txt")

    word_to_id = _build_vocab(train_path)
    train_data = _file_to_word_ids(train_path, word_to_id)
    test_data = _file_to_word_ids(test_path, word_to_id)
    valid_data = _file_to_word_ids(valid_path, word_to_id)
    vocabulary = len(word_to_id)
    return train_data, valid_data, test_data, vocabulary

def ptb_iterator(raw_data, batch_size, num_steps):
    raw_data = np.array(raw_data, dtype=np.int32)
    data_len = len(raw_data)
    batch_len = data_len // batch_size
    epoch_size = (batch_len - 1) // num_steps
    data = np.zeros([batch_size, batch_len], dtype=np.int32)
    for i in range(batch_size):
        data[i] = raw_data[batch_len * i : batch_len * (i + 1)]
    for i in range(epoch_size):


def main():
    datadir = "data"
    suffix = "train"
    filename = os.path.join(datadir, "ptb.%s.txt" % suffix)
    _build_vocab(filename)

if __name__ == "__main__":
    main()
