import os, random, sys, signal
import cPickle
from vocab import Vocab

def read_fren_data(config):

    if not os.path.isfile(config['train_src_ids']) or config['UPDATE_DATA'] is True:
        data_dir = config['DATA_DIR']
        num_lines = config['NUM_LINES']
        train_en = open(os.path.join(data_dir, "train.en")).readlines()[:num_lines]
        train_fr = open(os.path.join(data_dir, "train.fr")).readlines()[:num_lines]
        valid_en = open(os.path.join(data_dir, "valid.en")).readlines()[:num_lines]
        valid_fr = open(os.path.join(data_dir, "valid.fr")).readlines()[:num_lines]
        en_vocab = Vocab(train_en, config, config['src_vocab'])
        fr_vocab = Vocab(train_fr, config, config['trg_vocab'])
        train_en = [en_vocab.sentence2ids(s) for s in train_en]
        train_fr = [fr_vocab.sentence2ids(s) for s in train_fr]
        valid_en = [en_vocab.sentence2ids(s) for s in valid_en]
        valid_fr = [fr_vocab.sentence2ids(s) for s in valid_fr]
        cPickle.dump(train_en, open(config['train_src_ids'], 'wb'))
        cPickle.dump(train_fr, open(config['train_trg_ids'], 'wb'))
        cPickle.dump(valid_en, open(config['valid_src_ids'], 'wb'))
        cPickle.dump(valid_fr, open(config['valid_trg_ids'], 'wb'))
    else:
        train_en = cPickle.load(open(config['train_src_ids'], 'rb'))
        train_fr = cPickle.load(open(config['train_trg_ids'], 'rb'))
        valid_en = cPickle.load(open(config['valid_src_ids'], 'rb'))
        valid_fr = cPickle.load(open(config['valid_trg_ids'], 'rb'))
        en_vocab = Vocab(train_en, config, config['src_vocab'])
        fr_vocab = Vocab(train_fr, config, config['trg_vocab'])
    train_set = zip(train_en, train_fr)
    valid_set = zip(valid_en, valid_fr)
    return en_vocab, fr_vocab, train_set, valid_set 


def flipCoin(p):
    r = random.random()
    return r > p

def signal_handler(signal, frame):
        print('You pressed Ctrl+C!')
        sys.exit(0)

'''
def visualize_attention(self, src_sentence, trg_sentence):
    self.load()
    loss, attention_matrix = self.get_loss(src_sentence, trg_sentence)
    print [np.argmax(x) for x in attention_matrix]
    fig = plt.figure(figsize=(20, 20), dpi=80)
    plt.clf()
    matplotlib.rcParams.update({'font.size': 18})
    ax = fig.add_subplot(111)
    ax.set_aspect(1)
    ax.xaxis.tick_top()
    res = ax.imshow(attention_matrix, cmap=plt.cm.Blues, 
                    interpolation='nearest')
    
    # font_prop = FontProperties()
    # font_prop.set_file('./wqy-zenhei.ttf')
    # font_prop.set_size('large')
    # 
    # plt.xticks(range(len(alpha)), alpha, rotation=60, 
    #            fontproperties=font_prop)
    # plt.yticks(range(len(beta)), beta, fontproperties=font_prop)

    cax = plt.axes([0.0, 0.0, 0.0, 0.0])
    plt.colorbar(mappable=res, cax=cax)
    plt.show()
    #plt.savefig(name + '.png', format='png')
    #plt.close()
'''
