import sys, os, re, signal
import operator
import cPickle
import dynet as pc
import numpy as np
import matplotlib
from argparse import ArgumentParser
from progressbar import ProgressBar
from random import choice, randrange
from matplotlib import pyplot as plt
from nltk.translate.bleu_score import *
from vocab import Vocab
from util import *
from config import *
from multiprocessing import Pool

log_filename = 'log'


def main():
    config = default_config()

    en_vocab, fr_vocab, train_en, train_fr, valid_en, valid_fr = read_fren_data(config)
    train_set = zip(train_en, train_fr)
    valid_set = zip(valid_en, valid_fr)
    hyp = [fr_vocab.ids2sentence(x) for x in valid_fr]
    ref = zip(hyp, hyp, hyp)
    print corpus_bleu(ref, hyp)


if __name__ == '__main__':
    main()
