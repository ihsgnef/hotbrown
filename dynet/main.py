from argparse import ArgumentParser
from vocab import Vocab
from util import *
from config import *

from model import AttentionEncDec

log_filename = 'log'


def main():
    config = default_config()

    en_vocab, fr_vocab, train_set, valid_set = read_fren_data(config)

    rnn = AttentionEncDec(config, en_vocab, fr_vocab)

    if config['TEST']:
        bleu_score = rnn.test(valid_set)
        print bleu_score
    # elif config['VIZ']:
    #     rnn.visualize_attention(train_en[100], train_fr[100])

    rnn.train(train_set, valid_set, en_vocab, fr_vocab, config['NUM_EPOCHS'])


if __name__ == '__main__':
    main()
