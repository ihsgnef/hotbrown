def default_config():
    config = {}

    config['TRAIN'] = True
    config['TEST'] = False
    config['UPDATE_DATA'] = False
    config['RELOAD_MODEL'] = True
    config['MODEL_FILENAME'] = "attention.params"

    config['NUM_EPOCHS'] = 40
    config['BATCH_SIZE'] = 10

    config['ENC_NUM_LAYERS'] = 1
    config['DEC_NUM_LAYERS'] = 1
    config['ENC_STATE_SIZE'] = 64
    config['DEC_STATE_SIZE'] = 64
    config['ATTENTION_SIZE'] = 64
    config['EMBEDDING_SIZE'] = 300
    config['DROPOUT'] = 0.2

    config['DAGGER'] = False

    config['VOCAB_SIZE'] = 20000
    config['MAX_SENTENCE_LEN'] = 80
    config['NUM_LINES'] = 2000

    config['DATA_DIR'] = "/Users/fs/workspace/data/giga-fren/"
    config['src_vocab'] = config['DATA_DIR'] + "en.vocab." + str(config['VOCAB_SIZE'])
    config['trg_vocab'] = config['DATA_DIR'] + "fr.vocab." + str(config['VOCAB_SIZE'])
    config['train_src_ids'] = config['DATA_DIR'] + "train_en.ids." + str(config['NUM_LINES'])
    config['train_trg_ids'] = config['DATA_DIR'] + "train_fr.ids." + str(config['NUM_LINES'])
    config['valid_src_ids'] = config['DATA_DIR'] + "valid_en.ids." + str(config['NUM_LINES'])
    config['valid_trg_ids'] = config['DATA_DIR'] + "valid_fr.ids." + str(config['NUM_LINES'])
    return config
