import os
import dynet as pc
import numpy as np
import matplotlib
from progressbar import ProgressBar
from matplotlib import pyplot as plt
from nltk.translate.bleu_score import *
from util import *


class AttentionEncDec(object):


    def __init__(self, config,
                 src_vocab, trg_vocab, 
                 rnn_builder=pc.LSTMBuilder):

        self.src_vocab = src_vocab
        self.trg_vocab = trg_vocab
        self.SOS = self.src_vocab.SOS

        self.config = config

        self.batch_size = config['BATCH_SIZE']

        self.model_filename = config['MODEL_FILENAME']

        self.decode_input_size = config['ENC_STATE_SIZE'] * 2 + config['EMBEDDING_SIZE']

        self.embedding_size = config['EMBEDDING_SIZE']

        # DAGGER
        self.epsilon = 1.0

        self.model = pc.Model()

        self.src_lookup = self.model.add_lookup_parameters(
                (self.src_vocab.vocab_size, config['EMBEDDING_SIZE']))

        self.trg_lookup = self.model.add_lookup_parameters( 
                (self.trg_vocab.vocab_size, config['EMBEDDING_SIZE']))

        self.encode_fwd_RNN = rnn_builder(
                config['ENC_NUM_LAYERS'], config['EMBEDDING_SIZE'], 
                config['ENC_STATE_SIZE'], self.model)

        self.encode_bwd_RNN = rnn_builder(
                config['ENC_NUM_LAYERS'], config['EMBEDDING_SIZE'], 
                config['ENC_STATE_SIZE'], self.model)

        self.decode_RNN = rnn_builder(
                config['DEC_NUM_LAYERS'], self.decode_input_size,
                config['DEC_STATE_SIZE'], self.model)

        self.output_w = self.model.add_parameters( 
                (self.trg_vocab.vocab_size, config['DEC_STATE_SIZE']))

        self.output_b = self.model.add_parameters((self.trg_vocab.vocab_size))

        self.attention_w1 = self.model.add_parameters(
                (config['ATTENTION_SIZE'], config['ENC_STATE_SIZE'] * 2))

        self.attention_w2 = self.model.add_parameters(
                (config['ATTENTION_SIZE'], config['DEC_STATE_SIZE'] * config['DEC_NUM_LAYERS'] * 2))

        self.attention_v = self.model.add_parameters((1, config['ATTENTION_SIZE']))


    def load(self):
        if os.path.isfile(self.model_filename):
            self.model.load(self.model_filename)


    def save(self):
        self.model.save(self.model_filename)


    def get_probs(self, rnn_output):
        w = pc.parameter(self.output_w)
        b = pc.parameter(self.output_b)
        probs = pc.softmax(w * rnn_output + b)
        return probs

    def epsilon_decay(self):
        # linear decay 
        self.epsilon *= 0.99


    def embed_src_sentence(self, sentence):
        return [self.src_lookup[c] for c in sentence]


    def encode_sentence(self, fwd_init_state, bwd_init_state, input_vecs):
        fwd_states = fwd_init_state.add_inputs(input_vecs)
        bwd_states = bwd_init_state.add_inputs(input_vecs[::-1])
        fwd_outputs = [state.output() for state in fwd_states]
        bwd_outputs = [state.output() for state in bwd_states]
        outputs = [pc.concatenate([fwd, bwd]) for fwd, bwd in
                   zip(fwd_outputs, bwd_outputs)]
        return outputs


    def attend(self, encode_vecs, decode_state):
        w1 = pc.parameter(self.attention_w1)
        w2 = pc.parameter(self.attention_w2)
        v = pc.parameter(self.attention_v)
        weights = []
        w2dt = w2 * pc.concatenate(list(decode_state.s()))
        for enc_vec in encode_vecs:
            weight = v * pc.tanh(w1 * enc_vec + w2dt)
            weights.append(weight)
        weights = pc.softmax(pc.concatenate(weights))
        context_vec = pc.esum([vec * w for vec, w in zip(encode_vecs, weights)])
        return weights, context_vec


    def get_loss(self, sent_pair):
        src_sentence, trg_sentence = sent_pair
        embedded_src = self.embed_src_sentence(src_sentence)
        fwd_state = self.encode_fwd_RNN.initial_state()
        bwd_state = self.encode_bwd_RNN.initial_state()
        encode_vecs = self.encode_sentence(fwd_state, bwd_state, embedded_src)

        loss = []
        attention_matrix = []
        s = self.decode_RNN.initial_state()
        s = s.add_input(pc.vecInput(self.decode_input_size))
        # attention_weights, context_vec = self.attend(encode_vecs, s)
        # attention_matrix.append(attention_weights)
        init_input = pc.concatenate([encode_vecs[-1], self.trg_lookup[self.SOS]])
        s = s.add_input(init_input)
        for w in trg_sentence:
            output_vec = s.output()
            if self.config['DROPOUT'] > 0:
                output_vec = pc.dropout(output_vec, self.config['DROPOUT'])
            probs = self.get_probs(output_vec)
            predicted_w = np.argmax(probs)
            loss.append(-pc.log(pc.pick(probs, w)))
            attention_weights, context_vec = self.attend(encode_vecs, s)
            attention_matrix.append(attention_weights.value())
            if self.config['DAGGER'] and flipCoin(self.epsilon):
                w = predicted_w
            s = s.add_input(pc.concatenate([context_vec, self.trg_lookup[w]]))
        return pc.esum(loss)#, attention_matrix


    def generate(self, src_sentence):
        pc.renew_cg()

        embedded_src = self.embed_src_sentence(src_sentence)
        fwd_state = self.encode_fwd_RNN.initial_state()
        bwd_state = self.encode_bwd_RNN.initial_state()
        encode_vecs = self.encode_sentence(fwd_state, bwd_state, embedded_src)

        trg_sentence = []
        attention_matrix = []
        s = self.decode_RNN.initial_state()
        s = s.add_input(pc.vecInput(self.decode_input_size))
        # attention_weights, context_vec = self.attend(encode_vecs, s)
        # attention_matrix.append(attention_weights)
        init_input = pc.concatenate([encode_vecs[-1], self.trg_lookup[self.SOS]])
        s = s.add_input(init_input)
        s = s.add_input(init_input)
        while len(trg_sentence) < self.config['MAX_SENTENCE_LEN']:
            probs = self.get_probs(s.output())
            predicted_w = np.argmax(probs)
            trg_sentence.append(predicted_w)
            if predicted_w == self.trg_vocab.EOS:
                break
            attention_weights, context_vec = self.attend(encode_vecs, s)
            attention_matrix.append(attention_weights.value())
            s = s.add_input(pc.concatenate([context_vec, self.trg_lookup[predicted_w]]))
        return trg_sentence# , attention_matrix


    def train(self, train_set, valid_set, 
              src_vocab, trg_vocab, 
              num_epochs=20,):
    
        trainer = pc.AdagradTrainer(self.model)
        val_losses = []
        if self.config['RELOAD_MODEL']:
            self.load()

        for epoch in xrange(num_epochs):
            epoch_loss = 0
            progress = ProgressBar(max_value=len(train_set))
            num_batches = -(-len(train_set) // self.batch_size) 
            processed_examples = 0
            for b in xrange(num_batches):
                pc.renew_cg()
                batch = train_set[(b * self.batch_size) : ((b + 1) * self.batch_size)]

                batch_losses = [self.get_loss(pair) for pair in batch]

                batch_loss = pc.esum(batch_losses)
                epoch_loss += batch_loss.scalar_value()
                batch_loss.backward()
                trainer.update()

                processed_examples += self.batch_size
                progress.update(processed_examples)

            epoch_loss /= len(train_set)
            print '\r', epoch, epoch_loss

            if self.config['DAGGER']:
                self.epsilon_decay()

            with open(log_filename, 'a') as logfile:
                logfile.write(str(epoch) + ' ' + str(epoch_loss) + '\n')

            self.save()

    def test(self, test_set):
        self.load()
        hypothesis = []
        references = []
        for src_sent, trg_sent in test_set:
            ids = self.generate(src_sent)
            # both end with EOS
            hypothesis.append(self.trg_vocab.ids2sentence(ids[:-1]))
            references.append(self.trg_vocab.ids2sentence(trg_sent[:-1]))
        references = zip(references, references, references)
        return corpus_bleu(references, hypothesis)
