import sys, os, re
import operator
import cPickle
import numpy as np
from random import choice, randrange
from matplotlib import pyplot as plt

class Vocab:
    def __init__(self, data_set, config, filename):
        self._SOS = '<SOS>'
        self._EOS = '<EOS>'
        self._UNK = '<UNK>'
        self._PAD = '<PAD>'
        self.config = config
        self.vocab_size = config['VOCAB_SIZE']
        if os.path.isfile(filename) and config['UPDATE_DATA'] is False:
            self._int2word, self._word2int = cPickle.load(open(filename, 'rb'))
        else:
            word_count = dict()
            for sentence in data_set:
                for word in sentence.split():
                    if word not in word_count:
                        word_count[word] = 0
                    word_count[word] += 1
            words = sorted(word_count.items(), key=lambda x: x[1])
            self._int2word = [x[0] for x in words[::-1][:self.vocab_size - 4]]
            self._int2word += [self._UNK, self._SOS, self._EOS, self._PAD]
            self._word2int = {w:i for i, w in enumerate(self._int2word)}
            cPickle.dump([self._int2word, self._word2int], open(filename, 'wb'))
        self.SOS = self._word2int[self._SOS]
        self.EOS = self._word2int[self._EOS]
        self.UNK = self._word2int[self._UNK]
        self.PAD = self._word2int[self._PAD]
        
    def int2word(self, index):
        return self._int2word[index]

    def word2int(self, word):
        if word in self._word2int:
            return self._word2int[word]
        else:
            return self._word2int[self._UNK]

    def sentence2ids(self, sentence):
        regex = re.compile('[^a-zA-Z]')
        regex.sub('', sentence)
        sentence = sentence.lower().split()
        sentence = sentence + [self._EOS]
        return [self.word2int(w) for w in sentence] 

    def ids2sentence(self, ids):
        return ' '.join([self.int2word(i) for i in ids])

    def get_batchs(self, data_set, batch_size):
        data_set.sort(key=lambda x: -len(x))
        batchs = []
        i = 0
        while i < len(data_set):
            this_batch = data_set[i:i+batch_size]
            this_batch = [self.sentence2ids(s) for s in this_batch]
            max_len = max([len(x) for x in this_batch])
            for s in this_batch:
                s += [self.PAD] * (max_len - len(s))
            batchs.append(this_batch)
            i += batch_size
        return batchs
