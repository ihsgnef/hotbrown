import dynet as pc

class LSTM(object):

    number = 0

    def __init__(self, input_dims, output_dims, model):
        self.input_dims = input_dims
        self.output_dims = output_dims
        self.model = model

        LSTM.number += 1
        self.name = 'lstm_{}'.format(LSTM.number)

        self.W_i = self.model.add_parameters((output_dims, input_dims + output_dims))
        self.b_i = self.model.add_parameters((output_dims))
        self.W_f = self.model.add_parameters((output_dims, input_dims + output_dims))
        self.b_f = self.model.add_parameters((output_dims))
        self.W_c = self.model.add_parameters((output_dims, input_dims + output_dims))
        self.b_c = self.model.add_parameters((output_dims))
        self.W_o = self.model.add_parameters((output_dims, input_dims + output_dims))
        self.b_o = self.model.add_parameters((output_dims))
        self.c0 = self.model.add_parameters((output_dims))

        self.W_i.load_array(np.random.uniform(-0.01, 0.01, self.W_i.shape()))
        self.b_i.load_array(np.zeros(self.b_i.shape()))
        self.W_f.load_array(np.random.uniform(-0.01, 0.01, self.W_f.shape()))
        self.b_f.load_array(np.zeros(self.b_f.shape()))
        self.W_c.load_array(np.random.uniform(-0.01, 0.01, self.W_c.shape()))
        self.b_c.load_array(np.zeros(self.b_c.shape()))
        self.W_o.load_array(np.random.uniform(-0.01, 0.01, self.W_o.shape()))
        self.b_o.load_array(np.zeros(self.b_o.shape()))
        self.c0.load_array(np.zeros(self.c0.shape()))

    class State(object):
        
        def __init__(self, lstm):
            self.lstm = lstm

            self.outputs = []

            self.c = pc.parameter(self.lstm.c0)
            self.h = pc.tanh(self.c)

            self.W_i = pc.parameter(self.lstm.W_i)
            self.b_i = pc.parameter(self.lstm.b_i)
            self.W_f = pc.parameter(self.lstm.W_f)
            self.b_f = pc.parameter(self.lstm.b_f)
            self.W_c = pc.parameter(self.lstm.W_c)
            self.b_c = pc.parameter(self.lstm.b_c)
            self.W_o = pc.parameter(self.lstm.W_o)
            self.b_o = pc.parameter(self.lstm.b_o)

        def add_input(self, input_vec):

            x = pc.concatenate([input_vec, self.h])

            i = pc.logistic(self.W_i * x + self.b_i)
            f = pc.logistic(self.W_f * x + self.b_f)
            o = pc.logistic(self.W_o * x + self.b_o)
            g = pc.tanh(self.W_c * x + self.b_c)

            c = pycnn.cwise_multiply(f, self.c) + pycnn.cwise_multiply(i, g)
            h = pycnn.cwise_multiply(o, pycnn.tanh(c))

            self.c = c
            self.h = h
            self.outputs.append(h)

            return self
            
        def output(self):
            return self.outputs[-1]

    def initial_state(self):
        return LSTM.State(self)
