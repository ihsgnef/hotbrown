import sys, os, re
import operator
import pycnn as pc
import numpy as np
from random import choice, randrange
from matplotlib import pyplot as plt
from vocab import Vocab

NUM_LAYERS = 2
EMBEDDING_SIZE = 300
STATE_SIZE = 128
NUM_EPOCHS = 20
VOCAB_SIZE = 500
MAX_SENTENCE_LEN = 80
BATCH_SIZE = 10

RNN_BUILDER = pc.LSTMBuilder

data_dir = "/Users/fs/workspace/data/ptb/"

def read_data():
    data_sets = []
    for name in ['train', 'valid', 'test']:
        filename = "ptb.%s.txt" % name
        filepath = os.path.join(data_dir, filename)
        data = open(filepath).readlines()
        data_sets.append(data)
    return data_sets

class RNN(object):
    def __init__(self, vocab, num_layers, embedding_size, state_size):
        self.vocab = vocab
        self.model = pc.Model()
        self.model.add_lookup_parameters('lookup', 
                (self.vocab.vocab_size, embedding_size))
        self.RNN = RNN_BUILDER(num_layers, embedding_size, state_size,
                               self.model)
        self.model.add_parameters('output_w', 
                (self.vocab.vocab_size, state_size))
        self.model.add_parameters('output_b', (self.vocab.vocab_size))

    def _embed_sentence(self, sentence):
        lookup = self.model['lookup']
        return [lookup[c] for c in sentence]

    def _get_probs(self, rnn_output):
        output_w = pc.parameter(self.model['output_w'])
        output_b = pc.parameter(self.model['output_b'])
        probs = pc.softmax(output_w * rnn_output + output_b)
        return probs

    def _predict(self, probs):
        probs = probs.value()
        return self.vocab.int2word(np.argmax(probs))

    def run_RNN(self, init_state, input_vecs):
        s = init_state
        states = s.add_inputs(input_vecs)
        rnn_outputs = [s.output() for s in states]
        return rnn_outputs

    def get_loss(self, sentence):
        sentence = self.vocab.sentence2ids(sentence)

        # TODO why do I have to do this?
        pc.renew_cg()
        
        embedded_sentence = self._embed_sentence(sentence)
        rnn_state = self.RNN.initial_state()
        rnn_outputs = self.run_RNN(rnn_state, embedded_sentence)
        loss = []
        for rnn_output, output_word in zip(rnn_outputs, sentence):
            probs = self._get_probs(rnn_output)
            loss.append(-pc.log(probs[output_word]))
        return pc.esum(loss)

    def generate(self, sentence):
        sentence = self.vocab.sentence2ids(sentence)

        pc.renew_cg()

        embedded_sentence = self._embed_sentence(sentence)
        rnn_state = self.RNN.initial_state()
        rnn_outputs = self.run_RNN(rnn_state, embedded_sentence)

        output_sentence = []
        for rnn_output in rnn_outputs:
            probs = self._get_probs(rnn_output)
            predicted_word = self._predict(probs)
            output_sentence.append(predicted_word)

        output_sentence = ''.join(output_sentence)
        return output_sentence


def train(network, train_set, val_set, num_epochs=20):
    trainer = pc.SimpleSGDTrainer(network.model)
    val_losses = []
    train_set = train_set * num_epochs
    for i, sentence in enumerate(train_set):
        loss = network.get_loss(sentence)
        loss_value = loss.value()
        loss.backward()
        trainer.update()
        if i % (len(train_set) / 100) == 0:
            val_loss = 0
            for sentence in valid_set:
                loss = network.get_loss(sentence)
                loss_value = loss.value()
                val_loss += loss_value
            val_losses.append(val_loss)
            print i, val_loss

    plt.plot(val_losses)
    plt.show()



train_set, valid_set, test_set = read_data()
vocab = Vocab(train_set, VOCAB_SIZE)
# rnn = RNN(vocab, NUM_LAYERS, EMBEDDING_SIZE, STATE_SIZE)
# train(rnn, train_set, valid_set)
train_set = vocab.get_batchs(train_set, BATCH_SIZE)
